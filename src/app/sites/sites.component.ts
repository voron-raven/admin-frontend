import { Component, OnInit } from '@angular/core';

export interface Site {
  title: string;
  url: string;
  status: boolean;
}

const ELEMENT_DATA: Site[] = [
  {"title": "Admin", "url": "http://admin.mkeda.me", "status": false},
  {"title": "Airtickets", "url": "http://airtickets.mkeda.me", "status": false},
  {"title": "Chat", "url": "https://chat.mkeda.me", "status": false},
  {"title": "Games", "url": "https://games.mkeda.me", "status": false},
  {"title": "Jess blog", "url": "http://jkeda.me", "status": false},
  {"title": "Maps", "url": "https://maps.mkeda.me", "status": false},
  {"title": "Synergy", "url": "http://synergy.mkeda.me", "status": false},
  {"title": "Tools", "url": "https://tools.mkeda.me", "status": false},
  {"title": "Travels", "url": "http://mkeda.me", "status": false}
];


@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit {
  displayedColumns: string[] = ['status', 'url'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
